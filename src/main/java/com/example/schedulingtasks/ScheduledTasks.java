package com.example.schedulingtasks;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
	private static final String RESTART = "sudo service apache2 restart";
    private static final String DOCKERUP = "docker-compose up";
    private static final String DOCKERDOWN = "docker-compose down";

    @Scheduled(cron = "0 0 2 * * *")
    public void restart() {
        executeCommand(RESTART);
        executeCommand(DOCKERDOWN);
        executeCommand(DOCKERUP);
    }

    private void executeCommand(String command) {
        try {
            log(command);
            Process process = Runtime.getRuntime().exec(command);
            logOutput(process.getInputStream(), "");
            logOutput(process.getErrorStream(), "Error: ");
            int exitVal = process.waitFor();
            if (exitVal == 0) {
				log("Success!");
			}
			else {
				log("Fail!");
			}
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void logOutput(InputStream inputStream, String prefix) {
        new Thread(() -> {
            Scanner scanner = new Scanner(inputStream, "UTF-8");
            while (scanner.hasNextLine()) {
                synchronized (this) {
                    log(prefix + scanner.nextLine());
                }
            }
            scanner.close();
        }).start();
    }

    private synchronized void log(String message) {
        System.out.println(message);
    }
}