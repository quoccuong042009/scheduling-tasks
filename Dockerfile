FROM openjdk:8
LABEL maintainer="dinhhuy2808"
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /scheduling-tasks-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/scheduling-tasks-0.0.1-SNAPSHOT.jar"]